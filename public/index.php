<?php

require_once("../includes/initialize.php");

include_layout_template('header.php');

// look inside functions.php for a function __autoload()
// we would normally get an error for not requiring user.php here, the answer is there
$user = User::find_by_id(1);
echo $user->full_name();

echo "<hr>";

$users = User::find_all();
foreach ($users as $user) {
    echo "User: " . $user->username . "<br>";
    echo "Name: " . $user->full_name() . "<br><br>";
}

include_layout_template('footer.php');

?>
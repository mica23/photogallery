<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Dates and times: UNIX</title>
</head>
<body>
	<?php
		echo time();
		echo "<br>";
		echo mktime(2, 30, 45, 10, 1, 2016); // if the format is invalid, it will return false
		echo "<br>";
		echo checkdate(12, 31, 2000) ? 'true' : 'false';
		echo "<br>";
		echo checkdate(2, 31, 2000) ? 'true' : 'false';
		echo "<br>";

		$unix_timestamp = strtotime("+1 day");
		echo $unix_timestamp;
		echo "<br>";
	?>
</body>
</html>
<?php

$file = 'filetest.txt';
if($handle = fopen($file, 'w')) { // overwrite whatever we had in filetest.txt

	fwrite($handle, 'abc'); // return number of bytes written or false
	$content = "123\n456"; // double quotes matter (with \n)
	fwrite($handle, $content);

	fclose($handle); // we should always close files, PHP will close it in the and of the file, the same as database, again

	chmod($file, 0777); // we can't use the $handle
} else {
	echo "Could not open file for writing";
}

// file_put_contents: shortcut for fopen/fwrite/fclose
// overwrites existing file by default (so be CAREFUL)

$file = "filetest.txt";
$content = "111\n456\n789";

if ($size = file_put_contents($file, $content)) {
	echo "a file of size: {$size} bytes was created";
}

?>
<?php

$file = 'filetest.txt';
if($handle = fopen($file, 'w')) { // fopen returns a handle, similar to database connections
	fclose($handle); // we should always close files, PHP will close it in the and of the file, the same as database, again
} else {
	echo "Could not open file for writing";
};

?>
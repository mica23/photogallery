<?php

$filename = 'filetest.txt';

echo filesize($filename) . "<br>"; // in bytes

// filemtime: last modified (changed content)
// filectime: last changed (changed content or metadata)
// fileatime: last accessed (any read/change)

echo strftime("%m/%d/%Y %H:%M", filemtime($filename)) . "<br>";
echo strftime("%m/%d/%Y %H:%M", filectime($filename)) . "<br>";
echo strftime("%m/%d/%Y %H:%M", fileatime($filename)) . "<br>";

// touch($filename); // set values to current time

echo strftime("%m/%d/%Y %H:%M", filemtime($filename)) . "<br>";
echo strftime("%m/%d/%Y %H:%M", filectime($filename)) . "<br>";
echo strftime("%m/%d/%Y %H:%M", fileatime($filename)) . "<br>";

$path_parts = pathinfo(__FILE__);
print_r($path_parts); // get dirname, basename, filename, extension
?>